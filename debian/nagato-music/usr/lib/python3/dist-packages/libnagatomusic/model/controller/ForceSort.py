
from libnagatomusic.signal import Model as Signal
from libnagatomusic.model.controller.Controller import AbstractController


class NagatoForceSort(AbstractController):

    SIGNAL = Signal.FORCE_SORT_COLUMN

    def _control(self, user_data):
        yuki_column_id, yuki_sort_order = user_data
        yuki_model = self._enquiry("YUKI.N > base model")
        yuki_model.set_sort_column_id(yuki_column_id, yuki_sort_order)
