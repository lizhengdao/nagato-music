
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatomusic.signal import Model as Signal


class NagatoShift(NagatoObject):

    def _initialize(self):
        yuki_model = self._enquiry("YUKI.N > base model")
        if len(yuki_model) > 0:
            yuki_path = GLib.random_int_range(0, len(yuki_model))
            yuki_data = Signal.CURRENT_ROW_CHANGED, yuki_model[yuki_path]
            self._raise("YUKI.N > model signal", yuki_data)

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal == Signal.SHIFT:
            self._initialize()
        if yuki_signal == Signal.BASE_MODEL_REALIZED:
            self._initialize()

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > register model object", self)
