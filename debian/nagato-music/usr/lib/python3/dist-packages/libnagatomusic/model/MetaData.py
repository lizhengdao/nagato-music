
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatomusic.metadata.MetaData import HaruhiMetaData
from libnagatomusic.model import Columns


class NagatoMetaData(NagatoObject):

    def _set_row_data(self, tree_row):
        yuki_fullpath = tree_row[Columns.FULL_PATH]
        yuki_result = self._meta_data.get_from_fullpath(yuki_fullpath)
        yuki_title, yuki_artist, yuki_album = yuki_result
        tree_row[Columns.TITLE] = yuki_title
        tree_row[Columns.ARTIST] = yuki_artist
        tree_row[Columns.ALBUM] = yuki_album

    def _timeout(self):
        yuki_model = self._enquiry("YUKI.N > base model")
        self._set_row_data(yuki_model[self._index])
        self._index += 1
        return len(yuki_model) > self._index

    def parse(self):
        self._index = 0
        GLib.timeout_add(1, self._timeout)

    def __init__(self, parent):
        self._parent = parent
        self._meta_data = HaruhiMetaData()
