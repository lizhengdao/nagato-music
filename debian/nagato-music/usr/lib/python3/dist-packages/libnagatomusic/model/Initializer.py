
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatomusic.signal import Model as Signal
from libnagatomusic.model.DirectoryReader import NagatoDirectoryReader

DEFAULT_DIR = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_MUSIC)
# DEFAULT_DIR = "/home/takedanemuru/音楽/hololive"


class NagatoInitializer(NagatoObject):

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal == Signal.GUI_REALIZED:
            self._directory_reader.read_recursive(DEFAULT_DIR)
            self._raise("YUKI.N > read finished")

    def __init__(self, parent):
        self._parent = parent
        self._directory_reader = NagatoDirectoryReader(self)
        self._raise("YUKI.N > register model object", self)
