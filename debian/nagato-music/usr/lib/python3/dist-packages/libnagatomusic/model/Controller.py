
from libnagato4.Object import NagatoObject
from libnagatomusic.signal import Model as Signal
from libnagatomusic.model.Initializer import NagatoInitializer
from libnagatomusic.model.MetaData import NagatoMetaData
from libnagatomusic.model.Shift import NagatoShift
from libnagatomusic.model.receiver.RowActivated import NagatoRowActivated
from libnagatomusic.model.controller.ForceSort import NagatoForceSort


class NagatoController(NagatoObject):

    def _yuki_n_read_finished(self):
        self._meta_data.parse()
        yuki_data = Signal.BASE_MODEL_REALIZED, None
        self._raise("YUKI.N > model signal", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        NagatoInitializer(self)
        self._meta_data = NagatoMetaData(self)
        NagatoShift(self)
        NagatoRowActivated(self)
        NagatoForceSort(self)
