
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject


class NagatoPosition(NagatoObject):

    def _get_x(self, canvas_w, pixbuf_w):
        yuki_possibility = (canvas_w > (pixbuf_w+Unit(3))*2)
        self._raise("YUKI.N > possibility", yuki_possibility)
        if yuki_possibility:
            return (canvas_w)/2 - (pixbuf_w+Unit(1))
        return (canvas_w-pixbuf_w)/2

    def get_for_pixbuf(self, pixbuf):
        yuki_pixbuf_w, yuki_pixbuf_h = pixbuf.get_width(), pixbuf.get_height()
        _, _, yuki_canvas_w, yuki_canvas_h = self._enquiry("YUKI.N > geometry")
        yuki_x = self._get_x(yuki_canvas_w, yuki_pixbuf_w)
        yuki_y = (yuki_canvas_h-yuki_pixbuf_h)/2
        return yuki_x, yuki_y

    def __init__(self, parent):
        self._parent = parent
