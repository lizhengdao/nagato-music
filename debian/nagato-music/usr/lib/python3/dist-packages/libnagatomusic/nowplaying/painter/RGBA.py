
from gi.repository import Gdk
from libnagato4.Object import NagatoObject


class NagatoRGBA(Gdk.RGBA, NagatoObject):

    @classmethod
    def new_for_css_config_key(cls, parent, key):
        yuki_rgba = NagatoRGBA(parent)
        yuki_rgba.set_key(key)
        return yuki_rgba

    def set_key(self, key):
        self._key = key
        yuki_query = "css", self._key, "meaningless-default-value"
        yuki_settings = self._enquiry("YUKI.N > settings", yuki_query)
        self.parse(yuki_settings)

    def receive_transmission(self, user_data):
        yuki_group, yuki_key, yuki_value = user_data
        if yuki_group == "css" and yuki_key == self._key:
            self.parse(yuki_value)

    def __init__(self, parent):
        self._parent = parent
        Gdk.RGBA.__init__(self)
        self._raise("YUKI.N > register settings object", self)
