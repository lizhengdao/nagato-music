
from gi.repository import Gdk
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject
from libnagatomusic.nowplaying.pixbuf.Pixbuf import NagatoPixbuf
from libnagatomusic.nowplaying.pixbuf.Position import NagatoPosition


class NagatoCoverArt(NagatoObject):

    def _set_possibility(self):
        _, _, yuki_canvas_w, _ = self._enquiry("YUKI.N > geometry")
        yuki_possibility = (yuki_canvas_w > (200+Unit(3))*2)
        self._raise("YUKI.N > possibility", yuki_possibility)

    def paint(self, cairo_context):
        yuki_pixbuf = self._pixbuf.pixbuf
        if yuki_pixbuf is None:
            self._set_possibility()
            return
        yuki_x, yuki_y = self._position.get_for_pixbuf(yuki_pixbuf)
        Gdk.cairo_set_source_pixbuf(cairo_context, yuki_pixbuf, yuki_x, yuki_y)
        cairo_context.paint()

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = NagatoPixbuf(self)
        self._position = NagatoPosition(self)
