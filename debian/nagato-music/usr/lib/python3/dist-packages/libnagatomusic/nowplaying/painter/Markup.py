
from libnagato4.Object import NagatoObject
from libnagatomusic.model import Columns
from libnagatomusic.signal import Model as Signal
from libnagatomusic.metadata.MetaData import HaruhiMetaData


class NagatoMarkup(NagatoObject):

    def _refresh_markup(self, tree_row):
        yuki_path = tree_row[Columns.FULL_PATH]
        yuki_result = self._meta_data.get_from_fullpath(yuki_path)
        self._markup, yuki_artist, yuki_album = yuki_result
        self._markup += ("\n by "+yuki_artist) if yuki_artist else ""
        self._markup += ("\n in "+yuki_album) if yuki_album else ""

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal == Signal.CURRENT_ROW_CHANGED:
            self._refresh_markup(yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._meta_data = HaruhiMetaData()
        self._raise("YUKI.N > register model object", self)

    @property
    def markup(self):
        return self._markup
