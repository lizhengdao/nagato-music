
from libnagato4.Object import NagatoObject
from libnagatomusic.model import Columns
from libnagatomusic.signal import Model as Signal
from libnagatomusic.coverart.ScaledPixbuf import HaruhiScaledPixbuf


class NagatoPixbuf(NagatoObject):

    def _refresh_pixbuf(self, tree_row):
        yuki_fullpath = tree_row[Columns.FULL_PATH]
        yuki_file_type = tree_row[Columns.FILE_TYPE]
        self._pixbuf = self._scaled_pixbuf.get(yuki_fullpath, yuki_file_type)
        self._raise("YUKI.N > queue draw")

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal == Signal.CURRENT_ROW_CHANGED:
            self._refresh_pixbuf(yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = None
        self._scaled_pixbuf = HaruhiScaledPixbuf(150)
        self._raise("YUKI.N > register model object", self)

    @property
    def pixbuf(self):
        return self._pixbuf
