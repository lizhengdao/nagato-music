
from gi.repository import Gst
from libnagato4.Object import NagatoObject
from libnagatomusic.signal import Model as Signal
from libnagatomusic.model import Columns


class NagatoModel(NagatoObject):

    def _set_uri(self, tree_row):
        yuki_uri = tree_row[Columns.URI]
        yuki_playbin = self._enquiry("YUKI.N > playbin")
        yuki_playbin.set_state(Gst.State.READY)
        yuki_playbin.set_property("uri", yuki_uri)
        yuki_playbin.set_state(Gst.State.PLAYING)

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal == Signal.CURRENT_ROW_CHANGED:
            self._set_uri(yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > register model object", self)
