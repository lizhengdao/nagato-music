
from gi.repository import Gst
from libnagatomusic.signal import Model as ModelSignal
from libnagatomusic.signal import Playback as PlaybackSignal
from libnagatomusic.playback.controller.Controller import AbstractController

SEEK_FLAGS = Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT


class NagatoRewind(AbstractController):

    SIGNAL = PlaybackSignal.REWIND

    def _control(self, playbin, data=None):
        _, yuki_position = playbin.query_position(Gst.Format.TIME)
        if yuki_position/pow(1000, 3) >= 5:
            yuki_seek_data = Gst.Format.TIME, SEEK_FLAGS, 0
            playbin.seek_simple(*yuki_seek_data)
        else:
            yuki_data = ModelSignal.SHIFT, -1
            self._raise("YUKI.N > model signal", yuki_data)
