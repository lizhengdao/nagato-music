
from libnagato4.Object import NagatoObject


class AbstractController(NagatoObject):

    SIGNAL = "define signal to catch here."

    def _control(self, playbin, data=None):
        raise NotImplementedError

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal == self.SIGNAL:
            yuki_playbin = self._enquiry("YUKI.N > playbin")
            self._control(yuki_playbin, yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > register playback object", self)
