
from gi.repository import Gst
from libnagato4.Object import NagatoObject
from libnagatomusic.playback.controller.Controllers import AsakuraControllers
from libnagatomusic.playback.receiver.Receivers import AsakuraReceivers
from libnagatomusic.playback.watcher.Watchers import AsakuraWatchers


class NagatoPlaybin(NagatoObject):

    def _yuki_n_gst_property(self, user_data):
        yuki_key, yuki_value = user_data
        self._playbin.set_property(yuki_key, yuki_value)

    def _inform_playbin(self):
        return self._playbin

    def __init__(self, parent):
        self._parent = parent
        Gst.init()
        self._playbin = Gst.ElementFactory.make("playbin")
        self._playbin.set_property("flags", Gst.StreamType.AUDIO)
        self._playbin.no_more_pads()
        AsakuraControllers(self)
        AsakuraReceivers(self)
        AsakuraWatchers(self)
