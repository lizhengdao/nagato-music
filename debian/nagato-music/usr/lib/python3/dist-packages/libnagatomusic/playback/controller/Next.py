
from libnagatomusic.signal import Model as ModelSignal
from libnagatomusic.signal import Playback as PlaybackSignal
from libnagatomusic.playback.controller.Controller import AbstractController


class NagatoNext(AbstractController):

    SIGNAL = PlaybackSignal.NEXT

    def _control(self, playbin, data=None):
        yuki_data = ModelSignal.SHIFT, 1
        self._raise("YUKI.N > model signal", yuki_data)
