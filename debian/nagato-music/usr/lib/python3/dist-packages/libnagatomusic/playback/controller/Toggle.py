
from gi.repository import Gst
from libnagatomusic.signal import Playback as PlaybackSignal
from libnagatomusic.playback.controller.Controller import AbstractController


class NagatoToggle(AbstractController):

    SIGNAL = PlaybackSignal.TOGGLE

    def _control(self, playbin, data=None):
        _, yuki_state, _ = playbin.get_state(Gst.CLOCK_TIME_NONE)
        if yuki_state == Gst.State.PLAYING:
            playbin.set_state(Gst.State.PAUSED)
        else:
            playbin.set_state(Gst.State.PLAYING)
