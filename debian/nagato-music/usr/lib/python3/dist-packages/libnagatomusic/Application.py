
from libnagato4.application.Application import AbstractApplication
from libnagatomusic.mainline.MainWindow import NagatoMainWindow


class NagatoApplication(AbstractApplication):

    def _yuki_n_loopback_set_main_window(self, parent):
        print("YUKI.N > ...Ready ?")
        NagatoMainWindow(parent)
