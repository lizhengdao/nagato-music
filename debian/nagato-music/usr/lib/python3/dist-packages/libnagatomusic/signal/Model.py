
GUI_REALIZED = 0                # paired with None
CURRENT_ROW_CHANGED = 1         # paired with tree_row
BASE_MODEL_REALIZED = 2         # paired with None
SHIFT = 3                       # paired with integer
ROW_ACTIVATED = 4               # paired with row id as integer
FORCE_SORT_COLUMN = 5           # paired with order as integer
