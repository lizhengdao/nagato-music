
from libnagato4.Object import NagatoObject
from libnagatomusic.signal import Model as Signal


class NagatoModel(NagatoObject):

    def _set_cursor(self, tree_path):
        yuki_tree_view = self._enquiry("YUKI.N > listview")
        yuki_tree_view.set_cursor(tree_path, None, False)

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal == Signal.CURRENT_ROW_CHANGED:
            self._last_path = yuki_data.path
            self._set_cursor(yuki_data.path)

    def _on_map(self, *args):
        if self._last_path is not None:
            self._set_cursor(self._last_path)

    def __init__(self, parent):
        self._parent = parent
        yuki_tree_view = self._enquiry("YUKI.N > listview")
        self._last_path = None
        yuki_tree_view.connect("map", self._on_map)
        self._raise("YUKI.N > register model object", self)
