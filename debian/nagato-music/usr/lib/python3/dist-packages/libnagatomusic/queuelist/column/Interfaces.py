
from libnagatofiles.filer.column.Zebra import HaruhiZebra
from libnagatomusic.queuelist.column.Sortable import HaruhiSortable


class HaruhiInterfaces(HaruhiZebra, HaruhiSortable):

    def _bind_interfaces(self, renderer, sort_column_id):
        self._bind_zebra_column(renderer)
        self._bind_sortable_column(sort_column_id)
