
from libnagatomusic.queuelist.column.Title import NagatoTitle
from libnagatomusic.queuelist.column.Artist import NagatoArtist
from libnagatomusic.queuelist.column.Album import NagatoAlbum


class AsakuraColumns:

    def __init__(self, parent):
        NagatoTitle(parent)
        NagatoArtist(parent)
        NagatoAlbum(parent)
