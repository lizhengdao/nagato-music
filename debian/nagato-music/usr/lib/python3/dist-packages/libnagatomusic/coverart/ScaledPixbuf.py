
from gi.repository import GdkPixbuf
from libnagatomusic.coverart.Streams import HaruhiStreams

INTERP_TYPE = GdkPixbuf.InterpType.BILINEAR


class HaruhiScaledPixbuf:

    def _get_thumbnail_size(self, pixbuf):
        yuki_width = pixbuf.get_width()
        yuki_height = pixbuf.get_height()
        yuki_rate = min(yuki_width/self._size, yuki_height/self._size)
        return yuki_rate, (yuki_width/yuki_rate), (yuki_height/yuki_rate)

    def _get_scaled(self, pixbuf):
        yuki_rate, yuki_width, yuki_height = self._get_thumbnail_size(pixbuf)
        if 1 >= yuki_rate:
            return pixbuf
        return pixbuf.scale_simple(yuki_width, yuki_height, INTERP_TYPE)

    def get(self, fullpath, file_type):
        yuki_stream = self._streams.get_stream(fullpath, file_type)
        if yuki_stream is None:
            return None
        yuki_pixbuf = GdkPixbuf.Pixbuf.new_from_stream(yuki_stream, None)
        return self._get_scaled(yuki_pixbuf)

    def __init__(self, size):
        self._size = size
        self._streams = HaruhiStreams()
