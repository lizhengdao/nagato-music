
from libnagatothumbnail.mp4.Mutagen import HaruhiMutagen as Mp4
from libnagatothumbnail.mp3.Mutagen import HaruhiMutagen as Mp3


class HaruhiStreams:

    def get_stream(self, fullpath, file_type):
        if file_type == "audio/mp4":
            return self._mp4.get_stream_from_path(fullpath)
        elif file_type == "audio/mpeg":
            return self._mp3.get_stream_from_path(fullpath)
        return None

    def __init__(self):
        self._mp3 = Mp3()
        self._mp4 = Mp4()
