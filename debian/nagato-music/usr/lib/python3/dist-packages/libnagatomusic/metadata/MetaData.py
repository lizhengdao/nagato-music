
import mutagen
from gi.repository import GLib

TITLE = "TIT2", "\xa9nam"
ARTIST = "TPE1", "\xa9ART"
ALBUM = "TALB", "\xa9alb"


class HaruhiMetaData:

    def _has_key(self, mutagen_file, key):
        try:
            yuki_has_key = key in mutagen_file
            return yuki_has_key
        except ValueError:
            return False

    def _get_tag(self, mutagen_file, keys, default=""):
        for yuki_key in keys:
            if self._has_key(mutagen_file, yuki_key):
                return mutagen_file[yuki_key][0]
        return default

    def get_from_fullpath(self, fullpath):
        yuki_file = mutagen.File(fullpath)
        yuki_basename = GLib.path_get_basename(fullpath)
        yuki_title = self._get_tag(yuki_file, TITLE, yuki_basename)
        yuki_artist = self._get_tag(yuki_file, ARTIST)
        yuki_album = self._get_tag(yuki_file, ALBUM)
        return yuki_title, yuki_artist, yuki_album
