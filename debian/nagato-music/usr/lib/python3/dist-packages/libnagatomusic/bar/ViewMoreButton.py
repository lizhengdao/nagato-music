
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatomusic.bar.popover.ViewMorePopover import NagatoViewMorePopover

ICON_NAME = "view-more-symbolic"
SIZE = Gtk.IconSize.SMALL_TOOLBAR


class NagatoViewMoreButton(Gtk.Button, NagatoObject):

    def _on_clicked(self, button):
        self._popover.set_relative_to(self)
        self._popover.popup()
        self._popover.show_all()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        self._popover = NagatoViewMorePopover(self)
        yuki_image = Gtk.Image.new_from_icon_name(ICON_NAME, SIZE)
        self.set_image(yuki_image)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
