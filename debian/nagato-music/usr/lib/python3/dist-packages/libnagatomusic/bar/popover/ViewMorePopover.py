
from libnagato4.popover.SingleStack import AbstractSingleStack
from libnagatomusic.bar.popover.SwitchBox import NagatoSwitchBox


class NagatoViewMorePopover(AbstractSingleStack):

    def _add_to_single_stack(self):
        NagatoSwitchBox(self)
