
from libnagato4.mainline.MainGridLayer import AbstractMainGridLayer
from libnagatomusic.mainline.ModelLayer import NagatoModelLayer


class NagatoMainGridLayer(AbstractMainGridLayer):

    def _yuki_n_loopback_add_main_widget(self, parent):
        NagatoModelLayer(parent)
