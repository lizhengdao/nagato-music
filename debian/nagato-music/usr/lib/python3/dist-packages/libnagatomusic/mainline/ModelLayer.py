
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatomusic.model.BaseModel import NagatoBaseModel
from libnagatomusic.mainline.PlaybackLayer import NagatoPlaybackLayer


class NagatoModelLayer(NagatoObject):

    def _inform_filter_model(self):
        return self._filter_model

    def _yuki_n_model_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _yuki_n_register_model_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        self._base_model = NagatoBaseModel(self)
        self._filter_model = self._base_model.filter_new()
        NagatoPlaybackLayer(self)
