
from libnagato4.mainline.MainPopoverLayer import AbstractMainPopoverLayer
from libnagatomusic.mainline.HeaderLayer import NagatoHeaderLayer


class NagatoMainPopoverLayer(AbstractMainPopoverLayer):

    def _yuki_n_loopback_add_popover_item(self, parent_box):
        pass

    def _yuki_n_loopback_add_popover_group(self, parent_stack):
        pass

    def _on_initialize(self):
        NagatoHeaderLayer(self)
