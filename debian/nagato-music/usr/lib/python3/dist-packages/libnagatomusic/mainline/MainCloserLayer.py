
from libnagato4.maincloser.MainCloser import AbstractMainCloser
from libnagatomusic.mainline.HeaderNotifyLayer import NagatoHeaderNotifyLayer


class NagatoMainCloserLayer(AbstractMainCloser):

    def _yuki_n_loopback_set_content_closer(self, content_closer):
        pass

    def _yuki_n_confirm_to_close(self):
        self._standard_sequence()

    def _on_delete(self, widget, event):
        return self._standard_sequence()

    def _on_initialize(self):
        NagatoHeaderNotifyLayer(self)
