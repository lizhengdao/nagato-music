
from gi.repository import Gtk
from gi.repository import Pango
from libnagato4.Object import NagatoObject
from libnagatomusic.model import Columns
from libnagatomusic.queuelist.column.Interfaces import HaruhiInterfaces
from libnagatofiles.filer.column.RendererText import HaruhiRenderer


class NagatoArtist(Gtk.TreeViewColumn, NagatoObject, HaruhiInterfaces):

    def __init__(self, parent):
        self._parent = parent
        yuki_renderer = HaruhiRenderer.new(Pango.EllipsizeMode.MIDDLE)
        Gtk.TreeViewColumn.__init__(
            self,
            title="Artist",
            cell_renderer=yuki_renderer,
            text=Columns.ARTIST
            )
        self.set_expand(True)
        self._bind_interfaces(yuki_renderer, Columns.ARTIST)
        self._raise("YUKI.N > insert column", self)
