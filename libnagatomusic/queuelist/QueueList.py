
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatomusic.ui import StackName
from libnagatomusic.queuelist.ListView import NagatoListView


class NagatoQueueList(Gtk.ScrolledWindow, NagatoObject):

    def _yuki_n_add_to_scrolled_window(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self)
        self.set_hexpand(True)
        self.set_vexpand(True)
        NagatoListView(self)
        yuki_data = self, StackName.QUEUE_LIST
        self._raise("YUKI.N > add to stack named", yuki_data)
