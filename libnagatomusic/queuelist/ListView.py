
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatomusic.queuelist.column.Columns import AsakuraColumns
from libnagatomusic.queuelist.receiver.Model import NagatoModel

from libnagatomusic.signal import Model as Signal
from libnagatomusic.model import Columns


class NagatoListView(Gtk.TreeView, NagatoObject):

    def _yuki_n_insert_column(self, column):
        # "-1" means "add to last"
        self.insert_column(column, -1)

    def _on_row_activated(self, tree_view, tree_path, column):
        yuki_model = self.get_model()
        yuki_data = Signal.ROW_ACTIVATED, yuki_model[tree_path][Columns.ID]
        self._raise("YUKI.N > model signal", yuki_data)

    def _inform_listview(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        yuki_filter_model = self._enquiry("YUKI.N > filter model")
        Gtk.TreeView.__init__(self, model=yuki_filter_model)
        self.set_hexpand(True)
        self.set_vexpand(True)
        yuki_selection = self.get_selection()
        yuki_selection.set_mode(Gtk.SelectionMode.BROWSE)
        AsakuraColumns(self)
        NagatoModel(self)
        self.connect("row-activated", self._on_row_activated)
        self._raise("YUKI.N > add to scrolled window", self)
