
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatomusic.ui.MainVBox import NagatoMainVBox
from libnagatomusic.playback.Playbin import NagatoPlaybin


class NagatoPlaybackLayer(NagatoObject):

    def _yuki_n_playback_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _yuki_n_register_playback_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        NagatoPlaybin(self)
        NagatoMainVBox(self)
