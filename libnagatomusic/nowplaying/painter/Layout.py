
from gi.repository import Pango
from gi.repository import PangoCairo
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject
from libnagatomusic.nowplaying.painter import GtkFont
from libnagatomusic.nowplaying.painter.Markup import NagatoMarkup


class NagatoLayout(NagatoObject):

    def get_for_cairo_context(self, cairo_context):
        yuki_layout = PangoCairo.create_layout(cairo_context)
        yuki_layout.set_spacing(Pango.SCALE*Unit(1))
        yuki_layout.set_alignment(Pango.Alignment.LEFT)
        yuki_layout.set_markup(self._markup.markup)
        # yuki_layout.set_font_description(GtkFont.get(1, 400))
        yuki_layout.set_font_description(GtkFont.get(0, 400))
        _, _, yuki_canvas_w, yuki_canvas_h = self._enquiry("YUKI.N > geometry")
        yuki_layout.set_width(Pango.SCALE*(yuki_canvas_w/2-Unit(3)))
        _, yuki_logical_rect = yuki_layout.get_extents()
        yuki_text_height = yuki_logical_rect.height/Pango.SCALE
        yuki_x = yuki_canvas_w/2+Unit(1)
        yuki_y = (yuki_canvas_h-yuki_text_height)/2
        return yuki_layout, yuki_x, yuki_y

    def __init__(self, parent):
        self._parent = parent
        self._markup = NagatoMarkup(self)
