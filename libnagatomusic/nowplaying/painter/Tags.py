
from gi.repository import Gdk
from gi.repository import PangoCairo
from libnagato4.Object import NagatoObject
from libnagatomusic.nowplaying.painter.RGBA import NagatoRGBA
from libnagatomusic.nowplaying.painter.Layout import NagatoLayout

CSS_KEY = "primary_fg_color"


class NagatoTags(NagatoObject):

    def paint(self, cairo_context):
        if not self._enquiry("YUKI.N > possibility"):
            return
        Gdk.cairo_set_source_rgba(cairo_context, self._rgba)
        yuki_result = self._layout.get_for_cairo_context(cairo_context)
        yuki_layout, yuki_x, yuki_y = yuki_result
        cairo_context.move_to(yuki_x, yuki_y)
        PangoCairo.update_layout(cairo_context, yuki_layout)
        PangoCairo.show_layout(cairo_context, yuki_layout)

    def __init__(self, parent):
        self._parent = parent
        self._rgba = NagatoRGBA.new_for_css_config_key(self, CSS_KEY)
        self._layout = NagatoLayout(self)
