
from gi.repository import Gdk
from libnagato4.Object import NagatoObject
from libnagatomusic.nowplaying.painter.RGBA import NagatoRGBA

CSS_KEY = "primary_bg_color"


class NagatoBackground(NagatoObject):

    def paint(self, cairo_context):
        Gdk.cairo_set_source_rgba(cairo_context, self._rgba)
        yuki_geometry = self._enquiry("YUKI.N > geometry")
        cairo_context.rectangle(*yuki_geometry)
        cairo_context.fill()

    def __init__(self, parent):
        self._parent = parent
        self._rgba = NagatoRGBA.new_for_css_config_key(self, CSS_KEY)
