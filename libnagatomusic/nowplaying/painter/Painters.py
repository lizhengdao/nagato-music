
from libnagato4.Object import NagatoObject
from libnagatomusic.nowplaying.painter.Background import NagatoBackground
from libnagatomusic.nowplaying.painter.CoverArt import NagatoCoverArt
from libnagatomusic.nowplaying.painter.Tags import NagatoTags


class NagatoPainters(NagatoObject):

    def _yuki_n_possibility(self, possibility):
        self._possibility = possibility

    def _inform_possibility(self):
        return self._possibility

    def paint(self, cairo_context):
        self._background.paint(cairo_context)
        self._cover_art.paint(cairo_context)
        self._tags.paint(cairo_context)

    def __init__(self, parent):
        self._parent = parent
        self._possibility = True
        self._background = NagatoBackground(self)
        self._cover_art = NagatoCoverArt(self)
        self._tags = NagatoTags(self)
