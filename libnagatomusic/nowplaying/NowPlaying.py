
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatomusic.nowplaying.painter.Painters import NagatoPainters
from libnagatomusic.ui import StackName


class NagatoNowPlaying(Gtk.DrawingArea, NagatoObject):

    def _inform_geometry(self):
        yuki_rectangle, _ = self.get_allocated_size()
        return 0, 0, yuki_rectangle.width, yuki_rectangle.height

    def _yuki_n_queue_draw(self):
        self.queue_draw()

    def _on_draw(self, drawing_area, cairo_context):
        self._painters.paint(cairo_context)

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self)
        self.set_hexpand(True)
        self.set_vexpand(True)
        self._painters = NagatoPainters(self)
        self.connect("draw", self._on_draw)
        yuki_data = self, StackName.NOW_PLAYING
        self._raise("YUKI.N > add to stack named", yuki_data)
