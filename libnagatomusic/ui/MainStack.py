
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatomusic.nowplaying.NowPlaying import NagatoNowPlaying
from libnagatomusic.queuelist.QueueList import NagatoQueueList


class NagatoMainStack(Gtk.Stack, NagatoObject):

    def _yuki_n_add_to_stack_named(self, user_data):
        yuki_widget, yuki_name = user_data
        self.add_named(yuki_widget, yuki_name)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        self.set_transition_type(Gtk.StackTransitionType.OVER_LEFT_RIGHT)
        NagatoNowPlaying(self)
        NagatoQueueList(self)
        self._raise("YUKI.N > add to box", self)
