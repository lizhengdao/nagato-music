
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatomusic.ui.MainStack import NagatoMainStack
from libnagatomusic.bar.Bar import NagatoBar
from libnagatomusic.signal import Model as Signal


class NagatoMainVBox(Gtk.Box, NagatoObject):

    def _yuki_n_switch_stack_to(self, stack_name):
        self._stack.set_visible_child_name(stack_name)

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._stack = NagatoMainStack(self)
        NagatoBar(self)
        self._raise("YUKI.N > attach main widget", self)
        yuki_data = Signal.GUI_REALIZED, None
        self._raise("YUKI.N > model signal", yuki_data)
