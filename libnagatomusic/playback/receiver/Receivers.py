
from libnagatomusic.playback.receiver.Settings import NagatoSettings
from libnagatomusic.playback.receiver.Model import NagatoModel


class AsakuraReceivers:

    def __init__(self, parent):
        NagatoSettings(parent)
        NagatoModel(parent)
