
from libnagato4.Object import NagatoObject


class NagatoSettings(NagatoObject):

    def receive_transmission(self, user_data):
        yuki_type, yuki_key, yuki_value = user_data
        if yuki_type == "player" and yuki_key == "software_volume":
            yuki_data = "volume", float(yuki_value)
            self._raise("YUKI.N > gst property", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        yuki_query = "player", "software_volume", 0.2
        yuki_volume = self._enquiry("YUKI.N > settings", yuki_query)
        self._raise("YUKI.N > gst property", ("volume", yuki_volume))
        self._raise("YUKI.N > register settings object", self)
