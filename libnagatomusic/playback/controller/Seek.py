
from gi.repository import Gst
from libnagatomusic.signal import Playback as Signal
from libnagatomusic.playback.controller.Controller import AbstractController

SEEK_FLAGS = Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT


class NagatoSeek(AbstractController):

    SIGNAL = Signal.SEEK

    def _control(self, playbin, rate):
        _, yuki_duration = playbin.query_duration(Gst.Format.TIME)
        yuki_position = yuki_duration*rate
        yuki_seek_data = Gst.Format.TIME, SEEK_FLAGS, yuki_position
        playbin.seek_simple(*yuki_seek_data)
