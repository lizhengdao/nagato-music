
from libnagatomusic.playback.controller.Toggle import NagatoToggle
from libnagatomusic.playback.controller.Next import NagatoNext
from libnagatomusic.playback.controller.Rewind import NagatoRewind
from libnagatomusic.playback.controller.Seek import NagatoSeek


class AsakuraControllers:

    def __init__(self, parent):
        NagatoToggle(parent)
        NagatoNext(parent)
        NagatoRewind(parent)
        NagatoSeek(parent)
