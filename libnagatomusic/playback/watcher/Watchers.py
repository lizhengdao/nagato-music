
from libnagatomusic.playback.watcher.Bus import NagatoBus
from libnagatomusic.playback.watcher.Progress import NagatoProgress


class AsakuraWatchers:

    def __init__(self, parent):
        NagatoBus(parent)
        NagatoProgress(parent)
