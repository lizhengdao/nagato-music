
from gi.repository import Gst
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatomusic.signal import Playback as Signal


class NagatoProgress(NagatoObject):

    def _timeout(self, playbin):
        _, yuki_duration = playbin.query_duration(Gst.Format.TIME)
        _, yuki_position = playbin.query_position(Gst.Format.TIME)
        yuki_data = Signal.POSITION_UPDATED, (yuki_position, yuki_duration)
        self._raise("YUKI.N > playback signal", yuki_data)
        return GLib.SOURCE_CONTINUE

    def __init__(self, parent):
        self._parent = parent
        yuki_playbin = self._enquiry("YUKI.N > playbin")
        GLib.timeout_add(200, self._timeout, yuki_playbin)
