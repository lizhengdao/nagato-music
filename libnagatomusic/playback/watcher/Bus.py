
from libnagato4.Object import NagatoObject
from libnagatomusic.signal import Model as ModelSignal
from libnagatomusic.signal import Playback as PlaybackSignal


class NagatoBus(NagatoObject):

    def _on_state_changed(self, gst_bus, gst_message):
        yuki_old, yuki_new, yuki_pending = gst_message.parse_state_changed()
        yuki_data = PlaybackSignal.STATUS_CHANGED, yuki_new
        self._raise("YUKI.N > playback signal", yuki_data)

    def _on_eos(self, *args):
        yuki_data = ModelSignal.SHIFT, 1
        self._raise("YUKI.N > model signal", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        yuki_playbin = self._enquiry("YUKI.N > playbin")
        yuki_bus = yuki_playbin.get_bus()
        yuki_bus.add_signal_watch()
        yuki_bus.connect("message::eos", self._on_eos)
        yuki_bus.connect('message::state-changed', self._on_state_changed)
