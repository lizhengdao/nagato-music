
from libnagato4.Object import NagatoObject
from libnagatomusic.signal import Model as Signal
from libnagatomusic.model import Columns


class NagatoRowActivated(NagatoObject):

    def _control(self, id_):
        for yuki_row in self._enquiry("YUKI.N > base model"):
            if yuki_row[Columns.ID] == id_:
                yuki_data = Signal.CURRENT_ROW_CHANGED, yuki_row
                self._raise("YUKI.N > model signal", yuki_data)
                break

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal == Signal.ROW_ACTIVATED:
            self._control(yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > register model object", self)
