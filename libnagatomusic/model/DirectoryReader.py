
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoDirectoryReader(NagatoObject):

    def _read_file(self, directory, file_info):
        yuki_type = file_info.get_content_type()
        yuki_names = [directory, file_info.get_name()]
        yuki_path = GLib.build_filenamev(yuki_names)
        if yuki_type == "inode/directory":
            self.read_recursive(yuki_path)
        if yuki_type.startswith("audio"):
            yuki_uri = GLib.filename_to_uri(yuki_path)
            yuki_data = yuki_path, yuki_uri, yuki_type
            self._raise("YUKI.N > audio file found", yuki_data)

    def read_recursive(self, directory):
        yuki_gio_file = Gio.File.new_for_path(directory)
        for yuki_file_info in yuki_gio_file.enumerate_children("*", 0):
            self._read_file(directory, yuki_file_info)

    def __init__(self, parent):
        self._parent = parent
