
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatomusic.model import Columns
from libnagatomusic.model.Id import HaruhiId
from libnagatomusic.model.Controller import NagatoController


class NagatoBaseModel(Gtk.ListStore, NagatoObject):

    def _yuki_n_audio_file_found(self, user_data):
        self.append((self._id.get_new(), *user_data, "", "", ""))

    def _inform_base_model(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        self._id = HaruhiId()
        Gtk.ListStore.__init__(self, *Columns.TYPES)
        NagatoController(self)
