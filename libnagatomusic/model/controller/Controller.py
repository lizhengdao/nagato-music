
from libnagato4.Object import NagatoObject


class AbstractController(NagatoObject):

    SIGNAL = "define signal to handle here."

    def _control(self, user_data=None):
        raise NotImplementedError

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal == self.SIGNAL:
            self._control(yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > register model object", self)
