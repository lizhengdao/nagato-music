
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatomusic.Application import NagatoApplication


class NagatoYuki(NagatoObject):

    def _inform_library_directory(self):
        return GLib.path_get_dirname(__file__)

    def _request_synchronization(self, message, user_data=None):
        print("YUKI.N > message uncaught", message, user_data)

    def N(self, message):
        NagatoApplication(self)

    def __init__(self):
        self._parent = None
