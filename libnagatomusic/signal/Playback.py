
POSITION_UPDATED = 0    # paired with position and duration (float, float)
END_OF_STREAM = 1       # paired with None
PLAY = 2                # paired with None
PAUSE = 3               # paired with None
SEEK = 4                # paired with position rate as float
REWIND = 5              # paired with None
NEXT = 6                # paired with None
TOGGLE = 7              # paired with None
STATUS_CHANGED = 8      # paired with status as integer
