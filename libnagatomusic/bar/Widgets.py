
from libnagatomusic.bar.ViewMoreButton import NagatoViewMoreButton
from libnagatomusic.bar.BackwardButton import NagatoBackwardButton
from libnagatomusic.bar.ToggleButton import NagatoToggleButton
from libnagatomusic.bar.ForwardButton import NagatoForwardButton
from libnagatofiles.mediaviewer.seek.Bar import NagatoBar as SeekBar
from libnagatofiles.mediaviewer.volume.Button import NagatoButton


class AsakuraWidgets:

    def __init__(self, parent):
        NagatoViewMoreButton(parent)
        NagatoBackwardButton(parent)
        NagatoToggleButton(parent)
        NagatoForwardButton(parent)
        SeekBar(parent)
        NagatoButton(parent)
