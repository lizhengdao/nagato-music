
from gi.repository import Gst
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatomusic.signal import Playback as Signal

SIZE = Gtk.IconSize.SMALL_TOOLBAR
ICON_NAME = {
    Gst.State.PLAYING: "media-playback-pause-symbolic",
    Gst.State.PAUSED: "media-playback-start-symbolic"
    }


class NagatoToggleImage(Gtk.Image, NagatoObject):

    def receive_transmission(self, user_data):
        yuki_signal, yuki_data = user_data
        if yuki_signal == Signal.STATUS_CHANGED:
            if yuki_data in ICON_NAME:
                self.set_from_icon_name(ICON_NAME[yuki_data], SIZE)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self)
        self._raise("YUKI.N > image", self)
        self._raise("YUKI.N > register playback object", self)
