
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatomusic.ui import StackName

ICON_NAME = "view-list-symbolic"
ICON_SIZE = Gtk.IconSize.SMALL_TOOLBAR


class NagatoToQueueList(Gtk.Button, NagatoObject):

    def _on_clicked(self, button):
        self._raise("YUKI.N > switch stack to", StackName.QUEUE_LIST)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        yuki_image = Gtk.Image.new_from_icon_name(ICON_NAME, ICON_SIZE)
        self.set_image(yuki_image)
        self._raise("YUKI.N > css", (self, "popover-button"))
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
