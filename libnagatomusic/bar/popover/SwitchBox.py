
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatomusic.bar.popover.ToNowPlaying import NagatoToNowPlaying
from libnagatomusic.bar.popover.ToQueueList import NagatoToQueueList


class NagatoSwitchBox(Gtk.Box, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        NagatoToNowPlaying(self)
        NagatoToQueueList(self)
        self._raise("YUKI.N > add to box", self)
        self.show_all()
