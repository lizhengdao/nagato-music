
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatomusic.signal import Playback as Signal
from libnagatomusic.bar.ToggleImage import NagatoToggleImage


class NagatoToggleButton(Gtk.Button, NagatoObject):

    def _yuki_n_image(self, image):
        self.set_image(image)

    def _on_clicked(self, button):
        yuki_data = Signal.TOGGLE, None
        self._raise("YUKI.N > playback signal", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        NagatoToggleImage(self)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
