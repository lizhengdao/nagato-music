
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatomusic.signal import Playback as Signal

ICON_NAME = "media-seek-forward-symbolic"
SIZE = Gtk.IconSize.SMALL_TOOLBAR


class NagatoForwardButton(Gtk.Button, NagatoObject):

    def _on_clicked(self, button):
        yuki_data = Signal.NEXT, None
        self._raise("YUKI.N > playback signal", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        yuki_image = Gtk.Image.new_from_icon_name(ICON_NAME, SIZE)
        self.set_image(yuki_image)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
